![logo](http://dotfiles.github.io/images/dotfiles-logo.png)

# Mes dotfiles pour Archlinux

## Capture d'écran

![capture]()


## Détails

| **Fonction** | **Logiciels** |
| :--- | :---- |
| OS | Archlinux |
| Bureau | i3-gaps, Mate |
| Shell | Zsh |
| DM | Lightdm |
| Font | Terminus |
| Compositor | compton |
| Notification | Dunst |
| Terminal | termite |
| Lanceur | rofi |
| Barre | Polybar, i3bar + conky |
| Fond d'écran | feh |
| Verrouillage | lxlock |
| Déconnexion / Arrêt | lxlogout |
| Volume | amixer |
| Editeurs | vim, Atom |

## Liens
[Blog](https://draconis.me) <br />
[Mastodon](https://mstdn.io/@draconis) <br />
[Twitter](https://twitter.com/draconis3119)

## License
The code is available under the [License]()
