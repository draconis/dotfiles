#!/bin/bash
HOMEDIR=$HOME
USERNAME=$(whoami)


#installation de yay
echo "installation de yay"
sudo pacman -S git go
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

#dotfiles
yay -S vcsh
vcsh clone https://framagit.org/draconis/dotfiles.git dotfiles

#installation I3-WM
yay -Sy i3-gaps i3status py3status conky compton rofi fontawesome.sty
yay -S  lxappearance mpd mpc feh firefox firefox-i18n-fr termite ncmpcpp caja xarchiver
yay -S  numlockx

#vim vbundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

#theme icons
git clone https://github.com/horst3180/arc-icon-theme --depth 1 && cd arc-icon-theme ./autogen.sh --prefix=/usr
sudo make install
yay -S moka-icon-theme-git

#gtk theme
yay -S arc-gtk-theme lxappaerence
cd ~/.themes
git clone https://framagit.org/draconis/osx-arc-white.git

