# -*- mode: sh -*-
# Loaded by every zsh shell
# Do not put environement variables in it, use
# .profile for it.

#if [ "$TMUX" = "" ]; then tmux; fi
# * zplug
export ZPLUG_HOME=${HOME}/.zplug
if [[ ! -d ${ZPLUG_HOME} ]]; then
    git clone https://github.com/zplug/zplug $ZPLUG_HOME
fi

source ~/.zplug/init.zsh
zplug 'zplug/zplug', hook-build:'zplug --self-manage'

# ** prezto
# # *** tmux
#zplug "modules/tmux",       from:prezto
zstyle ':prezto:module:tmux:auto-start' local 'yes'

# # WAITING Break completion https://github.com/sorin-ionescu/prezto/issues/1245
# # should be fixed in zsh 5.3.2
# zplug "modules/editor",     from:prezto
# # Set the key mapping style to 'emacs' or 'vi'.
# zstyle ':prezto:module:editor' key-bindings 'emacs'
# # Auto convert .... to ../..
# zstyle ':prezto:module:editor' dot-expansion 'yes'

zplug "modules/history",    from:prezto

zplug "modules/utility",    from:prezto
zstyle ':prezto:module:utility:ls'    color 'yes'
zstyle ':prezto:module:utility:diff'  color 'yes'
zstyle ':prezto:module:utility:wdiff' color 'yes'
zstyle ':prezto:module:utility:make'  color 'yes'

# acces color by their name
zplug "modules/spectrum", from:prezto

zplug "modules/command-not-found", from:prezto, if:"[[ $OS != 'nixos' ]]"

zplug "modules/directory", from:prezto

# ** completions
# Additional completion definitions for Zsh.
zplug "zsh-users/zsh-completions", from:github

# ** fish like features
# Disable the fancies fish's features in Emacs
# *** History substring search
zplug "zsh-users/zsh-history-substring-search", from:github, \
      if:"[[ $TERM != 'dumb' ]]", \
      hook-load:"zmodload zsh/terminfo;
     bindkey "$terminfo[kcuu1]" history-substring-search-up;
     bindkey "$terminfo[kcud1]" history-substring-search-down;
     bindkey '^[[A' history-substring-search-up;
     bindkey '^[[B' history-substring-search-down;
     bindkey -M emacs '^P' history-substring-search-up;
     bindkey -M emacs '^N' history-substring-search-down;
     HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND='bg=5,fg=8,bold';
     HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND='bg=1,fg=8,bold'"

# ** theme
# Always use a theme except when TRAMP (Emacs) is used.
if [[ $TERM == "dumb" ]]; then
    PS1='$ '
fi
zplug "sindresorhus/pure", use:pure.zsh, from:github, as:theme, \
      if:"[[ $TERM != 'dumb' ]]", \
      hook-load:"PURE_GIT_PULL=0"
zplug "mafredri/zsh-async", from:github, on:"sindresorhus/pure"

# avit theme
#zplug "robbyrussell/oh-my-zsh", use:themes/avit.zsh-theme, from:github, as:theme
# Agnoster theme
#zplug "agnoster/agnoster-zsh-theme", use:agnoster.zsh-theme, from:github, as:theme
# Remove the prefix prompt when logged as draconis
#export DEFAULT_USER="draconis"

# # *** man pages
# # Solarized theme in man pages
zplug "zlsun/solarized-man", from:github

# dircolors
if [[ $OS != "macos" ]]; then
    if [[ ! -e "${HOME}/.local/share/dircolors/solarized" ]]; then
        git clone https://github.com/seebi/dircolors-solarized.git ${HOME}/.local/share/dircolors/solarized
    fi
    eval $(dircolors $HOME/.local/share/dircolors/solarized/dircolors.ansi-universal)
else
    export LSCOLORS=gxfxbEaEBxxEhEhBaDaCaD
fi

# # ** Packages
# # Packages installed via antibody when there is no correct way to
# # do it.

# # *** TODO git-subrepo
# # Install git-subrepo. I don't use the package manager for this as the
# # "make install" install method doesn't have command completion
# # working; as stated in the README.
# antibody bundle ingydotnet/git-subrepo
# # source ~/.cache/antibody/https-COLON--SLASH--SLASH-github.com-SLASH-ingydotnet-SLASH-git-subrepo/.rc

# *** Autosuggestion
# Sourcing zsh-syntax-highlighting need to be done before
# zsh-autosuggestions, otherwise this can cause problems.
zplug "zsh-users/zsh-autosuggestions", from:github, \
      if:"[[ $TERM != 'dumb' ]]", \
      hook-load:"ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=10';
      ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=(history-substring-search-up history-substring-search-down)"

# ** misc
# *** Syntax highlighting
zplug "zsh-users/zsh-syntax-highlighting", from:github, \
      if:"[[ $TERM != 'dumb' ]]"

# # *** notify when a long running command exit
zplug "marzocchi/zsh-notify", from:github

# ** apply zplug
# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose 2>&-; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load #--verbose

# * zsh config
# ** environment
# *** Smart URLs
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

# *** General
setopt BRACE_CCL          # Allow brace character class list expansion.
setopt COMBINING_CHARS    # Combine zero-length punctuation characters (accents)
# with the base character.
setopt RC_QUOTES          # Allow 'Henry''s Garage' instead of 'Henry'\''s Garage'.
unsetopt MAIL_WARNING     # Don't print a warning message if a mail file has been accessed.
setopt CHASE_LINKS        # Always use abolute path when cd with a symlink

# *** Jobs
setopt LONG_LIST_JOBS     # List jobs in the long format by default.
setopt AUTO_RESUME        # Attempt to resume existing job before creating a new process.
setopt NOTIFY             # Report status of background jobs immediately.
unsetopt BG_NICE          # Don't run all background jobs at a lower priority.
unsetopt HUP              # Don't kill jobs on shell exit.
unsetopt CHECK_JOBS       # Don't report on jobs when shell exit.

# *** direnv
eval "$(direnv hook zsh)"

# ** history
export HISTFILE="${ZDOTDIR:-$HOME}/.zhistory"       # The path to the history file.
export HISTSIZE=10000                   # The maximum number of events to save in the internal history.
export SAVEHIST=10000                   # The maximum number of events to save in the history file.

setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ':start:elapsed;command' format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire a duplicate event first when trimming history.
setopt HIST_IGNORE_DUPS          # Do not record an event that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete an old recorded event if a new event is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a previously found event.
setopt HIST_IGNORE_SPACE         # Do not record an event starting with a space.
setopt HIST_SAVE_NO_DUPS         # Do not write a duplicate event to the history file.
setopt HIST_VERIFY               # Do not execute immediately upon history expansion.
setopt HIST_BEEP                 # Beep when accessing non-existent history.

# ** emacs key bindings
bindkey -e

# * completion
# Enable completion
autoload -Uz compinit
compinit
# Use menu for choising correct completion
zstyle ':completion:*' menu select
# Enable completion for new installed command within the session by
# not trust the zsh's cache of executable command, and forced it be
zstyle ":completion:*:commands" rehash 1
# # *** TODO platformio
# if command -v platformio > /dev/null 2>&1; then
#     autoload bashcompinit && bashcompinit
#     eval "$(_PLATFORMIO_COMPLETE=source platformio)"
#     eval "$(_PLATFORMIO_COMPLETE=source pio)"
# fi


# * alias
# ** sudo
# Pass alias to sudo and prevent =nocorrect= errors
# https://superuser.com/a/749333
alias sudo='TERM=xterm-256color my_sudo '

function my_sudo {
    while [[ $# > 0 ]]; do
        case "$1" in
            command) shift ; break ;;
            nocorrect|noglob) shift ;;
            *) break ;;
        esac
    done
    if [[ $# = 0 ]]; then
        command sudo zsh
    else
        noglob command sudo $@
    fi
}

# Shortcut for emacs editor
alias ec='TERM=xterm-24bit emacsclient -t -a ""'
alias ecg='TERM=xterm-24bit emacsclient -nc -a ""'
alias ediff=emacs-diff

# ** rm
# Force the use of trash instead of rm
alias rm='echo -e "You are looking for *th*, not rm.
If you want to delete a file FOREVER use \\\rm."; false'
alias th='trash'

# ** ssh
alias ssh="TERM=xterm-256color ssh"
# Stop multiplexing of an host gracefully
alias sshs="ssh -O stop"
# Immediatlly terminate multiplexing of an host
alias sshx="ssh -O exit"

# ** ip
# Display only IPv4 or IPv6
alias ip4="ip -4"
alias ip6="ip -6"

# ** misc
alias yt="youtube-dl"
alias ff="firefox"
alias mpv="mpv --input-ipc-server=$HOME/.mpv.socket"
alias feh="feh --scale-down --auto-zoom -B black -g ''"
alias to="webtorrent"

# ** linux
if [[ "${OS}" != "macos" ]]; then
    # *** systemd
    alias sc="sudo systemctl"
    alias scu="systemctl --user"
    alias jc="sudo journalctl --system"
    alias jcu="journalctl --user"

    # *** misc
    alias tar="bsdtar"
    alias open="mimeo"
fi

# ** pacman
alias install="yay -S"
alias update="yay -Syu"
alias remove="yay -Rscn"

# git dotfiles
alias dotfiles="vcsh dotfiles"
